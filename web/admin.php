<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>PHPValladolid :: Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="admin_files/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="admin_files/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="admin_files/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<?php
    $users = array(
        array('nombre' => 'juan', 'apellidos' => 'pérez peréz', 'email' => 'jp@example.com', 'grupos'=>'[contabilidad,dirección]'),
        array('nombre' => 'pedro', 'apellidos' => 'garcía', 'email' => 'pgarcia@ejemplo.com', 'grupos'=>'[contabilidad,dirección]'),
        array('nombre' => 'maría', 'apellidos' => 'blanco', 'email' => 'nblanco@ejemplo.com', 'grupos'=>'[contabilidad]'),
    );
    $i = 0;
?>
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Charla/Taller testing con PHPUnit</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
              <li><a href="admin.php">Dashboard</a></li>
            <li><a href="#">Gestión de usuarios</a></li>
            <li><a href="#">Aplicaciones</a></li>
            <li><a href="#">Ayuda</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="#">Usuarios</a></li>
            <li><a href="#">Grupos</a></li>
            <li><a href="#">Informes</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="sub-header">Gestión de usuarios</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Email</th>
                  <th>Grupos</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo ++$i; ?></td>
                  <td><?php echo $user['nombre']; ?></td>
                  <td><?php echo $user['apellidos']; ?></td>
                  <td><?php echo $user['email']; ?></td>
                  <td><?php echo $user['grupos']; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="admin_files/jquery.js"></script>
    <script src="admin_files/bootstrap.js"></script>
    <!-- <script src="admin_files/docs.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="admin_files/ie10-viewport-bug-workaround.js"></script>

</body></html>