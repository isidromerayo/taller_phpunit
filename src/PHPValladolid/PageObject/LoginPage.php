<?php

namespace PHPValladolid\PageObject;

/**
 * Description of LoginPage
 *
 * @author isidromerayo
 */
class LoginPage {

    private $testCase;

    /**
     * 
     * @param type $testCase
     */
    public function __construct($testCase) {
        $this->testCase = $testCase;
    }

    /**
     * 
     * @param type $username
     * @param type $password
     */
    public function loginAs($username, $password) {
        $this->testCase->open('/');
        $this->testCase->type('username', $username);
        $this->testCase->type('password', $username, $password);
        $this->testCase->clickAndWait("//button[@type='submit']");
    }
    /**
     * 
     * @return boolean
     */
    public function isLoggedIn() {
        $this->testCase->assertTextPresent('Gestión de usuarios');
    }
}
