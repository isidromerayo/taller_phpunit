<?php

namespace PHPValladolid\Taller;

/**
 * Description of StringCalculator
 *
 * @author isidromerayo
 */
class StringCalculator {

    public function add($string) {
        $result = 0;
        $elements = $this->parse($string);
        foreach ($elements as $value) {
            $result += $value;
        }
        return $result;
    }
    
    private function parse($inputString) {
        $delimiter = ",";
        if (empty($inputString)) {
            return array();
        }
        if (preg_match('/^\/\//', $inputString)) {
            $delimiter = substr($inputString, 2,1);
            $inputString = substr($inputString, 5);
        }
        return explode($delimiter, str_replace("\n", $delimiter, $inputString));
    }
}
