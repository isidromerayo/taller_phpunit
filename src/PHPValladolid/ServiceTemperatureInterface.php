<?php

namespace PHPValladolid;

/**
 *
 * @author isidromerayo
 */
interface ServiceTemperatureInterface {
    public function readTemp();
}
