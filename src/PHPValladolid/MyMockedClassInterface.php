<?php

namespace PHPValladolid;

/**
 * Description of MyMockedClass
 *
 * @author isidromerayo
 */
interface MyMockedClassInterface {
    public function fooWithArgument();
}
