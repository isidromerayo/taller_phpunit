<?php

namespace PHPValladolid\Bank;

/**
 * Description of BankAccount
 *
 * @author isidromerayo
 */
class BankAccount {
    protected $balance = 0.00;

    protected $accountNumber = '';
    
    protected $pdo;

    /**
     * Initializes the bank account object.
     *
     * @param string $accountNumber
     * @param \PDO $pdo
     */
    public function __construct($accountNumber, \PDO $pdo)
    {
        $this->accountNumber = $accountNumber;
        $this->pdo = $pdo;
        
        $this->loadAccount();

    }
    /**
     * 
     * @param \PDO $pdo
     */
    static public function createTable(\PDO $pdo)
    {
        $query = "
            CREATE TABLE bank_account (
                account_number VARCHAR(17) PRIMARY KEY,
                balance DECIMAL(9,2) NOT NULL DEFAULT 0
            );
        ";

        $pdo->query($query);
    }
    
    protected function loadAccount()
    {
        $query = "SELECT * FROM bank_account WHERE account_number = ?";

        $statement = $this->pdo->prepare($query);

        $statement->execute(array($this->accountNumber));

        if ($bankAccountInfo = $statement->fetch(\PDO::FETCH_ASSOC))
        {
            $this->balance = $bankAccountInfo['balance'];
        }
        else
        {
            $this->balance = 0;
            $this->addAccount();
        }
    }

    /**
     * Adds account information to the database.
     */
    protected function addAccount()
    {
        $query = "INSERT INTO bank_account (balance, account_number) VALUES(?, ?)";

        $statement = $this->pdo->prepare($query);
        $statement->execute(array($this->balance, $this->accountNumber));
    }
}
