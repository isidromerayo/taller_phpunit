<?php

namespace PHPValladolid;

/**
 * Description of HelloWorld
 *
 * @author isidromerayo
 */
class HelloWorld {
    /**
     * 
     * @param String $text
     */
    public function sayHello($text = ''){
        return 'Hello ' . $text;
    }
}
