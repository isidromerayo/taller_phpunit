<?php

namespace PHPValladolid;

/**
 * Description of Calculator
 *
 * @author isidromerayo
 */
class Calculator {

    public function __construct() {
        
    }
    /**
     * 
     * @param int $value
     * @return float
     * @throws Exception
     */
    public function inverse($value) {
         if (!$value) {
            throw new \InvalidArgumentException('Division by zero.');
        }
        return 1/$value;
    }
}
