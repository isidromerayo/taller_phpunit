<?php
class CyliconVallerTallerTest extends \PHPUnit_Extensions_SeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser("*chrome");
    $this->setBrowserUrl("http://www.cyliconvalley.es/");
  }

  public function testMyTestCase()
  {
    $this->open("/");
    $this->verifyText("link=Taller testing y PHPUnit.", "Taller testing y PHPUnit.");
    $this->click("link=Taller testing y PHPUnit.");
    $this->waitForPageToLoad("30000");
    $this->verifyText("css=span.comment-count", "0 comentarios");
  }
}