<?php

namespace Functional\PHPValladolid\WebDriver;

/**
 * Description of GitHubTests
 *
 * @author isidromerayo
 */
class GitHubTests extends \PHPUnit_Framework_TestCase {
    /**
     * @var \RemoteWebDriver
     */
    protected $webDriver;

    protected $url = 'https://github.com';
    
    public function setUp()
    {
        $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
        $this->webDriver = \RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
    }

    public function testGitHubHome()
    {
        $this->webDriver->get($this->url);
        
        $this->assertContains('GitHub', $this->webDriver->getTitle());
    } 
    /** @test */
    public function searching()
    {
        $this->webDriver->get($this->url);
        $search = $this->webDriver->findElement(\WebDriverBy::cssSelector('.js-site-search-form > input:nth-child(2)'));
        $search->click();
        $this->assertContains('GitHub', $this->webDriver->getTitle());
    }   
    /** 
     * @test
     * @group demo 
     */
    public function clickOnSearch()
    {
        $this->webDriver->get($this->url);
        $search = $this->webDriver->findElement(\WebDriverBy::cssSelector('.js-site-search-form > input:nth-child(2)'));
        $search->click();
        // typing into field
        $this->webDriver->getKeyboard()->sendKeys('php-webdriver');
        // pressing "Enter"
        $this->webDriver->getKeyboard()->pressKey(\WebDriverKeys::ENTER);
        $this->assertContains("Search · php-webdriver · GitHub",$this->webDriver->getTitle());
    }
    
    public function tearDown()
    {
        $this->webDriver->close();
    }
}
