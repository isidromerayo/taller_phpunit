<?php

namespace Functional\PHPValladolid;

/**
 * Description of PHPProTest
 *
 * @author isidromerayo
 */
class PHPProTest extends \PHPUnit_Extensions_SeleniumTestCase {

    public static $browsers = array(
        array(
            'name' => 'Linux Firefox',
            'browser' => '*firefox',
            'host' => 'localhost',
            'port' => 4444,
            'timeout' => 30000,
        ),
//        array(
//            'name' => 'Linux Opera',
//            'browser' => '*opera',
//            'host' => 'localhost',
//            'port' => 4444,
//            'timeout' => 30000,
//        ),
        array(
            'name' => 'Linux Chrome',
            'browser' => '*chrome',
            'host' => 'localhost',
            'port' => 4444,
            'timeout' => 30000,
        ),
//        array(
//            'name' => 'MacOSX Safari',
//            'browser' => '*safari',
//            'host' => 'my.macosx.box',
//            'port' => 4444,
//            'timeout' => 30000,
//        ),
//        array(
//            'name' => 'Windows Safari',
//            'browser' => '*custom C:\Program Files\Safari\Safari.exe -url',
//            'host' => 'my.windowsxp.box',
//            'port' => 4444,
//            'timeout' => 30000,
//        ),
//        array(
//            'name' => 'Windows IE',
//            'browser' => '*iexplore',
//            'host' => 'my.windowsxp.box',
//            'port' => 4444,
//            'timeout' => 30000,
//        )
    );

    protected function setUp() {
        $this->setBrowserUrl('http://www.phpro.org');
    }

    public function testTitle() {
        $this->open('/');
        $this->assertTitle('PHPRO.ORG - PHP Websites Tutorials Articles Classes Services');
    }

}
