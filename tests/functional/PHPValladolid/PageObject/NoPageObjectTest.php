<?php

namespace Functional\PHPValladolid\PageObject;

/**
 * Description of PageObjectTest
 *
 * @author isidromerayo
 */
class NoPageObjectTest extends \PHPUnit_Extensions_SeleniumTestCase {

    private $login = 'demo@localhost';
    private $password = 'password';
    protected $url = 'http://localhost:8000';

    public function setUp() {
        $this->setBrowser("*chrome");
        $this->setBrowserUrl($this->url);
    }

    /**
     *
     * @test
     */
    public function canLogin() {
        $this->open('/');
        $this->type('username', $this->login);
        $this->type('password', $this->password);
        $this->clickAndWait("//button[@type='submit']");
        $this->waitForPageToLoad();
        $this->assertTextPresent('Gestión de usuarios');
    }

    public function tearDown() {
        parent::tearDown();
    }

}
