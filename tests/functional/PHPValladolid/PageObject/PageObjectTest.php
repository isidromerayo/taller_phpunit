<?php

namespace Functional\PHPValladolid\PageObject;

/**
 * Description of PageObjectTest
 *
 * @author isidromerayo
 */
class PageObjectTest extends \PHPUnit_Extensions_SeleniumTestCase {
    private $login = 'demo@localhost';
    private $password = 'password';
    
    protected $url = 'http://localhost:8000';
    
    public function setUp()
    {
        $this->setBrowser("*chrome");
        $this->setBrowserUrl($this->url);
    }
    /**
    * @test
    */
    public function canLogin() {
         $loginPage = new \PHPValladolid\PageObject\LoginPage($this);
         $loginPage->loginAs($this->login,$this->password);
         $loginPage->isLoggedIn();
    }

    public function tearDown() {
        parent::tearDown();
    }
}
