<?php

namespace Integration\PHPValladolid;

/**
 * Description of ArticleDAOTest
 *
 * @author isidromerayo
 */
class DatabaseSQLiteInMemoryTest extends \PHPUnit_Framework_TestCase {

    private $messages = array(
            array('title' => 'Hello!',
                'message' => 'Just testing...',
                'time' => 1327301464),
            array('title' => 'Hello again!',
                'message' => 'More testing...',
                'time' => 1339428612),
            array('title' => 'Hi!',
                'message' => 'SQLite3 is cool...',
                'time' => 1327214268)
        );
    private $db;

    protected function setUp() {
        // Create new database in memory
        $this->db = new \PDO('sqlite::memory:');
        // Set errormode to exceptions
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->prepareDatabaseAndData();
    }

    private function prepareDatabaseAndData() {
        $this->db->exec("CREATE TABLE IF NOT EXISTS messages (
                      id INTEGER PRIMARY KEY, 
                      title TEXT, 
                      message TEXT, 
                      time TEXT)");
        
        // Prepare INSERT statement to SQLite3 memory db
        $insert = "INSERT INTO messages ( title, message, time) 
                VALUES ( :title, :message, :time)";
        $stmt = $this->db->prepare($insert);

        foreach ($this->messages as $m) {
            // Bind values directly to statement variables
            $stmt->bindValue(':title', $m['title'], SQLITE3_TEXT);
            $stmt->bindValue(':message', $m['message'], SQLITE3_TEXT);

            // Format unix time to timestamp
            $formatted_time = date('Y-m-d H:i:s', $m['time']);
            $stmt->bindValue(':time', $formatted_time, SQLITE3_TEXT);

            // Execute statement
            $stmt->execute();
        }
    }
    public function testGetMessages() {
        $result = $this->db->query('SELECT * FROM messages')->fetchAll();
        $this->assertCount(3, $result);
    }

}
