<?php

namespace Integration\PHPValladolid;

/**
 * Description of DataSetAssertionsTest
 *
 * @author isidromerayo
 */
class DataSetAssertionsTest extends \PHPUnit_Extensions_Database_TestCase {
    
    private $pdo;
    public function setUp() {
        //  GRANT ALL PRIVILEGES ON test.* TO 'demo'@'localhost' IDENTIFIED BY 'demo'; FLUSH PRIVILEGES;
        $this->pdo = new \PDO('mysql:host=localhost;dbname=test', 'demo', 'demo');
        $tableMetaData = 'CREATE TABLE IF NOT EXISTS  guestbook (id INTEGER, content TEXT, user VARCHAR(255) )';
        $this->pdo->exec($tableMetaData);
        // parent::setUp();
    }
    /** @todo */
    public function manualDataSetAssertion() {
        // In my Debian 7 no works :|
        $dataSet = new \PHPUnit_Extensions_Database_DataSet_QueryDataSet($this->getConnection());
        $dataSet->addTable('guestbook', 'SELECT id, content, user FROM guestbook');
        $expectedDataSet = $this->createFlatXmlDataSet(dirname(__FILE__) . '/_files/guestbook.xml');
        $this->assertDataSetsEqual($expectedDataSet, $dataSet);
    }
    
    public function testDummy(){
        $this->assertFalse(FALSE);
    }

    /**
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    public function getConnection() {
        return $this->createDefaultDBConnection($this->pdo, 'test');
    }

    /**
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet() {
        return $this->createFlatXmlDataSet(dirname(__FILE__) . '/_files/guestbook.xml');
    }

}
