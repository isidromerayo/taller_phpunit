<?php

namespace Integration\PHPValladolid;

/**
 * Description of BankAccountDBTest
 *
 * @author isidromerayo
 */
class BankAccountDBTest extends \PHPUnit_Extensions_Database_TestCase
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO('sqlite::memory:');
        \PHPValladolid\Bank\BankAccount::createTable($this->pdo);
    }

    protected function getConnection()
    {
        return $this->createDefaultDBConnection($this->pdo, 'sqlite');
    }

    protected function getDataSet()
    {
        return $this->createFlatXMLDataSet(dirname(__FILE__).'/_files/bank-account-seed.xml');
    }

    public function testNewAccountCreation()
    {
        $bank_account = new \PHPValladolid\Bank\BankAccount('12345678912345678', $this->pdo);
 
        $xml_dataset = $this->createFlatXMLDataSet(dirname(__FILE__).'/_files/bank-account-after-new-account.xml');
        $this->assertTablesEqual($xml_dataset->getTable('bank_account'), $this->getConnection()->createDataSet()->getTable('bank_account'));
    }
}
