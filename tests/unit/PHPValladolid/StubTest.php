<?php

namespace Unit\PHPValladolid;

use \Mockery as m;

/**
 * Description of StubTest
 *
 * @author isidromerayo
 */
class StubTest extends \PHPUnit_Framework_TestCase {

    /**
     * @test 
     * @group Mockery
     */
    public function getsAverageTemperatureFromThreeServiceReadingsMockeryExample() {
        $service = m::mock('\PHPValladolid\ServiceTemperatureInterface');
        $service->shouldReceive('readTemp')->times(3)->andReturn(10, 12, 14);
        $temperature = new \PHPValladolid\Temperature($service);
        $this->assertEquals(12, $temperature->average());
    }

    /**
     * @test
     * @group PHPUnit
     */
    public function getsAverageTemperatureFromThreeServiceReadingsPHPUnitExample() {
        $service = $this->getMock('\PHPValladolid\ServiceTemperatureInterface', array('readTemp'));

        $service->expects($this->any())
                ->method('readTemp')
                ->will($this->onConsecutiveCalls(10, 12, 14));
        $temperature = new \PHPValladolid\Temperature($service);
        $this->assertEquals(12, $temperature->average());
    }

    /**
     * @test 
     * @group Phake
     */
    public function getsAverageTemperatureFromThreeServiceReadings() {
        // We need define a Service Interface
        $service = \Phake::mock('\PHPValladolid\ServiceTemperatureInterface');
        \Phake::when($service)->readTemp()->thenReturn(10)
                ->thenReturn(12)
                ->thenReturn(14);

        $temperature = new \PHPValladolid\Temperature($service);
        $this->assertEquals(12, $temperature->average());
    }

    /**
     * @test
     * @expectedException OutOfBoundsException
     * @group PHPUnit
     */
    public function throwExceptionStubPHPUnit()
    {
        $stub = $this->getMock('\PHPValladolid\SomeClass');
 
        $stub->expects($this->any())
             ->method('doSomething')
             ->will($this->throwException(new \OutOfBoundsException()));
        $stub->doSomething();
    }
    /**
     * @test
     * @expectedException \OutOfBoundsException
     * @group Mockery 
     */
    public function throwExceptionStubMockery() {
        $stub = m::mock('\PHPValladolid\SomeClass');
        $stub->shouldReceive('doSomething')->andThrow(new \OutOfBoundsException());
        $stub->doSomething();
    }
    /**
     * @test
     * @expectedException OutOfBoundsException
     * @group Phake 
     */
    public function throwExceptionStubPhake() {
        $stub = \Phake::mock('\PHPValladolid\SomeClass');
 
        \Phake::when($stub)->doSomething()
                            ->thenThrow(new \OutOfBoundsException());
        $stub->doSomething();
    }
}