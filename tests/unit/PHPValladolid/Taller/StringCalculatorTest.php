<?php

namespace Unit\PHPValladolid\Taller;

/**
 * Description of StringCalculatorTest
 *
 * @author isidromerayo
 */
class StringCalculatorTest extends \PHPUnit_Framework_TestCase {
    
    private $calculator;
    
    public function setUp() {
        $this->calculator = new \PHPValladolid\Taller\StringCalculator();
    }

    public function testAddEmptyStringShouldBeReturnZero() {
        $result = $this->calculator->add("");
        $expected = 0;
        $this->assertSame($expected, $result);
    }
    
    public function testaAddOneNumberShouldBeReturnShameNumber(){
        $result = $this->calculator->add("1");
        $expected = 1;
        $this->assertEquals($expected, $result);
    }
    
    public function testAddTwoNumbersShouldBeReturnTheSum() {
        $result = $this->calculator->add("1,2");
        $expected = 3;
        $this->assertEquals($expected, $result);
    }
    public function testAddWithLineFeedReturnTheSum(){
        $result = $this->calculator->add("1\n2,3");
        $expected = 6;
        $this->assertEquals($expected, $result);
    }
    // innecesary?¿?
    public function testAddWithMultipleLineFeedReturnTheSum(){
        $result = $this->calculator->add("1\n2,3\n4");
        $expected = 10;
        $this->assertEquals($expected, $result);
    }
    
    public function exampleData() {
        return array(
            array("",0),
            array("4",4),
            array("4,-2",2),
            array("1\n2\n3\n4",10)
        );
    }
    /**
     * @dataProvider exampleData
     */
    public function testUsingDataProvider($data, $expected) {
        $result = $this->calculator->add($data);
        $this->assertEquals($expected, $result);
    }
    
    public function testDiferentDelimiters() {
        $result = $this->calculator->add('//;\n1;2');
        $expected = 3;
        $this->assertEquals($expected, $result);
    }
}
