<?php

namespace Unit\PHPValladolid;

use Mockery as m;
/**
 * Description of MockTest
 *
 * @author isidromerayo
 */
class MockTest extends \PHPUnit_Framework_TestCase {
    /**
     * @test
     * @group Phake
     */
    public function mockExamplePhake() {
        $mock = \Phake::mock('\PHPValladolid\MyMockedClassInterface');
 
        $mock->fooWithArgument('foo');
        $mock->fooWithArgument('bar');
 
        \Phake::inOrder(
        \Phake::verify($mock)->fooWithArgument('foo'),
        \Phake::verify($mock)->fooWithArgument('bar')
        );
    }
    /**
     * @test
     * @group PHPUnit
     */
    public function mockExamplePHPUnit() {
         $mock = $this->getMock('\PHPValladolid\MyMockedClassInterface');
 
        $mock->expects($this->at(0))->method('fooWithArgument')->with('foo');
        $mock->expects($this->at(1))->method('fooWithArgument')->with('bar');
 
        $mock->fooWithArgument('foo');
        $mock->fooWithArgument('bar');
    }
    /**
     * @test
     * @group Mockery
     */
    public function mockExampleMockery() {
        $mock = m::mock('\PHPValladolid\MyMockedClassInterface');
 
        $mock->shouldReceive('fooWithArgument')->with('foo')->ordered();
        $mock->shouldReceive('fooWithArgument')->with('bar')->ordered();
        
        $mock->fooWithArgument('foo');
        $mock->fooWithArgument('bar');
        // Strict mode
        $this->assertTrue(TRUE);
    }
}
