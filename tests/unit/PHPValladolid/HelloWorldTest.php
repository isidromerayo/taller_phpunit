<?php

namespace Unit\PHPValladolid;

/**
 * Description of HelloWorldTest
 *
 * @author isidromerayo
 */
class HelloWorldTest extends \PHPUnit_Framework_TestCase {
    
    public function testHelloWorld() {
        // Arrange
        $object = new \PHPValladolid\HelloWorld();
        // Act
        $result = $object->sayHello('PHPValladolid');
        // Assert
        $this->assertEquals('Hello PHPValladolid', $result);
        // $this->assertNotEmpty($result);
    }
    /** @test */
    public function should_be_say_hello_girls_and_boys() {
        // Arrange
        $object = new \PHPValladolid\HelloWorld();
        // Act
        $result = $object->sayHello('Girls & Boys');
        // Assert
        $this->assertEquals('Hello Girls & Boys', $result);
    } 
}
