<?php

namespace Unit\PHPValladolid;

/**
 * Description of StackTest
 *
 * @author isidromerayo
 */
class StackTest extends \PHPUnit_Framework_TestCase {

    private $stack;
    
    public function setUp() {
        $this->stack = array();
    }
    /** @test */
    public function should_be_zero_elements_by_default() {
        $this->assertEquals(0, count($this->stack));
        $this->assertCount(0, $this->stack);
    }
    /** @test */
    public function should_be_one_element_when_push_something() {
        array_push($this->stack, 'foo');

        $this->assertEquals('foo', $this->stack[count($this->stack)-1]);
        $this->assertCount(1, $this->stack);
    }
    /** @test */
    public function should_be_zero_elements_when_push_and_pop() {
        
        array_push($this->stack, 'foo');
        array_pop($this->stack);
        
        $this->assertCount(0, $this->stack);
    }
    
}