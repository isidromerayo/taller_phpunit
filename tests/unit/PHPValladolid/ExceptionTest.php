<?php

namespace Unit\PHPValladolid;

/**
 * Description of ExceptionTest
 *
 * @author isidromerayo
 */
class ExceptionTest extends \PHPUnit_Framework_TestCase {
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $calculator = new \PHPValladolid\Calculator();
        $calculator->inverse(0);
    }
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Division by zero.
     */
    public function testMessageException()
    {
        $calculator = new \PHPValladolid\Calculator();
        $calculator->inverse(0);
    }
}
